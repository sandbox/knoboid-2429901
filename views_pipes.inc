<?php
/**
 * @file
 * Helper functions.
 */

/**
 * Update a database record.
 *
 * @param int $vpid
 *   Views pipe database ID.
 * @param array $views_pipe
 *   An array containing the views pipe data to be updated.
 */
function views_pipes_db_update($vpid, array $views_pipe) {
  unset($views_pipe['vpid']);
  db_update('views_pipes')->fields($views_pipe)->condition('vpid', $vpid)->execute();
}

/**
 * Get a database record.
 *
 * @param int $vpid
 *   Views pipe database ID.
 */
function views_pipes_get_views_pipe($vpid) {
  $result = db_select('views_pipes', 'v')->fields('v')->condition('vpid', $vpid)->execute()->fetchAssoc();
  return $result;
}

/**
 * Load views and iterate through each updating the data array $views_data.
 *
 * @return array
 *   List of all views and displays which contain fields and arguments.
 */
function views_pipes_get_views_data() {
  $views = views_get_all_views();
  $views_data = array();
  foreach ($views as $view) {
    if ($view->disabled) {
      continue;
    }
    views_pipes_get_view_data($view, $views_data);
  }
  return $views_data;
}

/**
 * Extract necessary data from the $view object.
 *
 * Extract data about displays, fields and arguments from the $view object
 * and build the $views_data array in a form that can be easily interrogated
 * by the views pipe configuration form.
 *
 * @param object $view
 *   Configuration data for a view, as provided by the views module.
 * @param array $views_data
 *   Hierarchical array of data describing all views, displays, views fields
 *   and views arguments organized for populating form select elements.
 */
function views_pipes_get_view_data($view, &$views_data = array()) {
  $view_name = $view->name;
  $vid = &$view->vid;
  $human_name = &$view->human_name;
  foreach ($view->display as $display_name => &$display_data) {
    $has_args = isset($display_data->display_options['arguments']);
    $args = $has_args ? $display_data->display_options['arguments'] : array();
    if ($display_name == 'default') {
      // Master (default) display.
      $default_args = $has_args ? $args : array();
    }
    else {
      // Not the default display.
      $args = $has_args ? $args : $default_args;
    }
    $argument_count = format_plural(count($args), '1 argument', '@count arguments');
    if ($display_name != 'default' && count($args) != 0) {
      $views_data[$view_name]['input_display_options'][$display_name] = $display_data->display_title . ' (' . $display_name . ') : ' . $argument_count;
    }
    $arg_position = 0;
    $views_data[$view_name][$display_name]['arguments']['select_options'] = array();
    foreach ($args as $arg_name => $arg_data) {
      $arg_postion_str = t('arg no. @arg_no', array('@arg_no' => ++$arg_position));
      $ui_name = isset($arg_data['ui_name']) ? $arg_data['ui_name'] : '';
      $arg_identifier = $ui_name == '' ? $arg_name : $ui_name;
      $views_data[$view_name][$display_name]['arguments']['select_options'][$arg_name] = $arg_identifier . ' (' . $arg_data['table'] . ':' . $arg_data['field'] . ':' . $arg_postion_str . ')';
      $views_data[$view_name][$display_name]['arguments']['type'][$arg_name] = $arg_data['table'] . ':' . $arg_data['field'];
    }
    if (isset($views_data[$view_name]['input_display_options'])) {
      $views_data['input_view_options'][$view_name] = $human_name . ' (' . $view_name . ')';
    }

    // Do the same for fields.
    $has_fields = isset($display_data->display_options['fields']);
    $fields = $has_fields ? $display_data->display_options['fields'] : array();
    $has_style_plugin = isset($display_data->display_options['style_plugin']);
    $style_plugin = $has_style_plugin ? $display_data->display_options['style_plugin'] : '';
    if ($display_name == 'default') {
      // Master (default) display.
      $default_fields = $has_fields ? $fields : array();
      $default_style_plugin = $style_plugin;
    }
    else {
      // Not the default display.
      $fields = $has_fields ? $fields : $default_fields;
      $style_plugin = $has_style_plugin ? $style_plugin : $default_style_plugin;
    }
    // For output views, only use displays using the 'table' style plugin.
    if ($style_plugin != 'table') {
      continue;
    }
    $field_count = format_plural(count($fields), '1 field', '@count fields');
    if ($display_name != 'default') {
      $views_data[$view_name]['output_display_options'][$display_name] = $display_data->display_title . ' (' . $display_name . ') : ' . $field_count;
    }
    $field_position = 0;
    $views_data[$view_name][$display_name]['fields']['select_options'] = array();
    foreach ($fields as $field_name => $field_data) {
      $field_postion_str = t('field no. @field_no', array('@field_no' => ++$field_position));
      $ui_name = isset($field_data['ui_name']) ? $field_data['ui_name'] : '';
      $field_identifier = $ui_name == '' ? $field_name : $ui_name;
      $views_data[$view_name][$display_name]['fields']['select_options'][$field_name] = $field_identifier . ' (' . $field_data['table'] . ':' . $field_data['field'] . ':' . $field_postion_str . ')';
      $views_data[$view_name][$display_name]['fields']['type'][$field_name] = $field_data['table'] . ':' . $field_data['field'];
    }
    if (isset($views_data[$view_name]['output_display_options'])) {
      $views_data['output_view_options'][$view_name] = $human_name . ' (' . $view_name . ')';
    }
  }
  unset($views_data[$view_name]['default']);
  return $views_data;
}

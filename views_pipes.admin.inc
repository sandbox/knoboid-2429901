<?php
/**
 * @file
 * Functions associated with the administration interface.
 */

/**
 * Page callback for the views pipe form.
 *
 * @param int $vpid
 *   Views Pipse DB id.
 * @param string $action
 *   Either 'edit' or 'clone'. If NULL, this is an add form.
 *
 * @return array
 *   A form array describing the views pipe add/edit/clone form.
 */
function views_pipes_views_pipe_form_callback($vpid = NULL, $action = NULL) {
  module_load_include('inc', 'views_pipes', 'views_pipes');
  $views_pipe = NULL;
  if (isset($action)) {
    $views_pipe = views_pipes_get_views_pipe($vpid);
    if ($views_pipe == FALSE) {
      return MENU_NOT_FOUND;
    }
  }
  return drupal_get_form('views_pipes_views_pipe_form', $views_pipe, $action);
}

/**
 * Form constructor for the views pipes add/edit/clone form.
 *
 * @ingroup forms
 *
 * @see views_pipes_views_pipe_form_submit()
 * @see views_pipes_views_pipe_form_validate()
 * @see views_pipes_settings_ajax_callback()
 *
 * @todo Theme layer.
 */
function views_pipes_views_pipe_form($form, &$form_state, $views_pipe = NULL, $action = NULL) {
  module_load_include('inc', 'views_pipes', 'views_pipes');

  // For a new form $updated is FALSE. After AJAX refreshes it is TRUE.
  $updated = isset($form_state['values']);

  $is_edit_or_clone_form = isset($action);

  if ($action == 'edit') {
    $form['vpid'] = array('#type' => 'hidden', '#value' => $views_pipe['vpid']);
    $page_title = t('<em>Edit Views Pipe</em> @title', array(
      '@title' => $views_pipe['name'],
    ));
  }
  elseif ($action == 'clone') {
    $page_title = t('<em>Cloning Views Pipe </em> @title', array(
      '@title' => $views_pipe['name'],
    ));
    // Prevent repeated updating of name.
    if (!$updated) {
      $views_pipe['name'] = t('@pipe_name (cloned)', array('@pipe_name' => $views_pipe['name']));
    }
  }
  else {
    $page_title = t('<em>Add Views Pipe</em>');
  }

  // There must be a better way to reset the page title after failed validation
  // than setting a hidden field. ?
  $form['page_title'] = array('#type' => 'hidden', '#value' => $page_title);
  drupal_set_title($page_title, PASS_THROUGH);

  $views_data = views_pipes_get_views_data();

  if ($is_edit_or_clone_form) {
    $enabled_default = $views_pipe['active'] ?
      array('active' => 'active') :
      array('active' => 0);
  }
  else {
    $enabled_default = array('active' => 'active');
  }

  $enabled_options = array('active' => t('If unchecked this views pipe will not alter the output view.'));

  // Logic for output view select lists.
  // Options and default for the Output Display.
  // If no output results in views.
  if ($has_no_fields = !isset($views_data['output_view_options'])) {
    $no_results = t('No results');
    $output_view_options = array(0 => $no_results);
  }
  else {
    $output_view_options = $views_data['output_view_options'];
  }

  if ($output_view_options == array()) {
    $no_results = isset($no_results) ? $no_results : t('No results');
    $output_view_options = $output_display_options = $output_field_options = array(0 => $no_results);
  }
  if ($updated) {
    $output_view = $form_state['values']['output_view'];
  }
  elseif ($is_edit_or_clone_form) {
    $output_view = $views_pipe['output_view'];
  }
  else {
    $output_view = key($output_view_options);
  }

  // Options and default for the Output Display.
  // If no output results in views.
  $output_display_options = $has_no_fields ? $output_view_options :
    $views_data[$output_view]['output_display_options'];

  if ($updated) {
    if (array_key_exists($form_state['values']['output_display'], $output_display_options)) {
      $output_display = $form_state['values']['output_display'];
    }
    else {
      $output_display = key($output_display_options);
    }
  }
  elseif ($is_edit_or_clone_form) {
    $output_display = $views_pipe['output_display'];
  }
  else {
    $output_display = key($output_display_options);
  }

  // Options and default for the Output Field.
  // If no output results in views.
  $output_field_options = $has_no_fields ? $output_view_options :
    $views_data[$output_view][$output_display]['fields']['select_options'];
  if ($updated) {
    if (array_key_exists($form_state['values']['output_field'], $output_field_options)) {
      $output_field = $form_state['values']['output_field'];
    }
    else {
      $output_field = key($output_field_options);
    }
  }
  elseif ($is_edit_or_clone_form) {
    $output_field = $views_pipe['output_field'];
  }
  else {
    $output_field = key($output_field_options);
  }

  // Logic for input view select lists.
  // Options and default for the Input View.
  // If no input results in views.
  if ($has_no_arguments = !isset($views_data['input_view_options'])) {
    $no_results = isset($no_results) ? $no_results : t('No results');
    $input_view_options = array(0 => $no_results);
  }
  else {
    $input_view_options = $views_data['input_view_options'];
  }

  if ($updated) {
    $input_view = $form_state['values']['input_view'];
  }
  elseif ($is_edit_or_clone_form) {
    $input_view = $views_pipe['input_view'];
  }
  else {
    $input_view = key($input_view_options);
  }

  // Options and default for the Input Display.
  // If no input results in views.
  $input_display_options = $has_no_arguments ? $input_view_options :
    $views_data[$input_view]['input_display_options'];

  if ($updated) {
    if (array_key_exists($form_state['values']['input_display'], $input_display_options)) {
      $input_display = $form_state['values']['input_display'];
    }
    else {
      $input_display = key($input_display_options);
    }
  }
  elseif ($is_edit_or_clone_form) {
    $input_display = $views_pipe['input_display'];
  }
  else {
    $input_display = key($input_display_options);
  }

  // Options and default for the Input Argument (Contextual Filter).
  // If no input results in views.
  $input_argument_options = $has_no_arguments ? $input_view_options :
    $views_data[$input_view][$input_display]['arguments']['select_options'];

  if ($updated) {
    if (array_key_exists($form_state['values']['input_argument'], $input_argument_options)) {
      $input_argument = $form_state['values']['input_argument'];
    }
    else {
      $input_argument = key($input_argument_options);
    }
  }
  elseif ($is_edit_or_clone_form) {
    $input_argument = $views_pipe['input_argument'];
  }
  else {
    $input_argument = key($input_argument_options);
  }

  // Messages related to the settings.
  // Show either 'No results', 'Mismatch' or 'OK'.
  if ($has_no_fields || $has_no_arguments) {
    // Couldn't get any results from views for input or output (or both).
    if ($has_no_fields) {
      $messages[] = t('No results: <em>Views has no table displays containing fields.</em>');
      $type = 'warning';
      $form['no_fields'] = array('#type' => 'hidden', '#value' => TRUE);
    }
    if ($has_no_arguments) {
      $messages[] = t('No results: <em>Views has no displays with contextual filters set.</em>');
      $type = 'warning';
      $form['no_arguments'] = array('#type' => 'hidden', '#value' => TRUE);
    }
    $message = implode('<br />', $messages);
    $form['no_results'] = array('#type' => 'hidden', '#value' => TRUE);
  }
  else {
    // Check for mismatch between field and argument.
    $output_field_type = $views_data[$output_view][$output_display]['fields']['type'][$output_field];
    $form['output_field_type'] = array('#type' => 'hidden', '#value' => $output_field_type);
    $input_argument_type = $views_data[$input_view][$input_display]['arguments']['type'][$input_argument];
    $form['input_argument_type'] = array('#type' => 'hidden', '#value' => $input_argument_type);

    if ($output_field_type != $input_argument_type) {
      $message = t("Warning! Mismatch between output field %field_type and input argument %argument_type.", array('%field_type' => $output_field_type, '%argument_type' => $input_argument_type));
      $type = 'warning';
    }
    else {
      $message = t("Argument and field match: %field_type.", array('%field_type' => $output_field_type));
      $type = 'status';
    }
  }

  // Buid the form.
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Views Pipe Name'),
    '#default_value' => $is_edit_or_clone_form ? $views_pipe['name'] : '',
    '#size' => 60,
    '#maxlength' => 256,
    '#required' => TRUE,
  );

  $form['views_pipe_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['views_pipe_settings']['settings-wrapper'] = array(
    '#prefix' => '<div id="settings-wrapper" class="admin clearfix">',
    '#suffix' => '</div>',
  );

  $form['views_pipe_settings']['settings-wrapper']['output'] = array(
    '#type' => 'fieldset',
    '#title' => t('Output View'),
    '#prefix' => '<div id="output-wrapper" class="left clearfix">',
    '#suffix' => '</div>',
  );

  $form['views_pipe_settings']['settings-wrapper']['input'] = array(
    '#type' => 'fieldset',
    '#title' => t('Input View'),
    '#prefix' => '<div id="input-wrapper" class="right clearfix">',
    '#suffix' => '</div>',
  );

  $form['views_pipe_settings']['settings-wrapper']['output']['output_view'] = array(
    '#type' => 'select',
    '#title' => t('View'),
    '#options' => $output_view_options,
    '#default_value' => $output_view,
    '#description' => t('Only views in which fields have been set appear in the list.'),
    '#ajax' => array(
      'wrapper' => 'settings-wrapper',
      'callback' => 'views_pipes_settings_ajax_callback',
    ),
  );

  $form['views_pipe_settings']['settings-wrapper']['output']['output_display'] = array(
    '#type' => 'select',
    '#title' => t('Display'),
    '#options' => $output_display_options,
    '#default_value' => $output_display,
    '#ajax' => array(
      'wrapper' => 'settings-wrapper',
      'callback' => 'views_pipes_settings_ajax_callback',
    ),
  );

  $form['views_pipe_settings']['settings-wrapper']['output']['output_field'] = array(
    '#type' => 'select',
    '#title' => t('Field providing output'),
    '#options' => $output_field_options,
    '#default_value' => $output_field,
    '#ajax' => array(
      'wrapper' => 'settings-wrapper',
      'callback' => 'views_pipes_settings_ajax_callback',
    ),
  );

  $form['views_pipe_settings']['settings-wrapper']['input']['input_view'] = array(
    '#type' => 'select',
    '#title' => t('View'),
    '#options' => $input_view_options,
    '#default_value' => $input_view,
    '#description' => t('Only views in which contextual filters have been set appear in the list.'),
    '#ajax' => array(
      'wrapper' => 'settings-wrapper',
      'callback' => 'views_pipes_settings_ajax_callback',
    ),
  );

  $form['views_pipe_settings']['settings-wrapper']['input']['input_display'] = array(
    '#type' => 'select',
    '#title' => t('Display'),
    '#options' => $input_display_options,
    '#default_value' => $input_display,
    '#ajax' => array(
      'wrapper' => 'settings-wrapper',
      'callback' => 'views_pipes_settings_ajax_callback',
    ),
  );

  $form['views_pipe_settings']['settings-wrapper']['input']['input_argument'] = array(
    '#type' => 'select',
    '#title' => t('Contextual Filter receiving input'),
    '#options' => $input_argument_options,
    '#default_value' => $input_argument,
    '#ajax' => array(
      'wrapper' => 'settings-wrapper',
      'callback' => 'views_pipes_settings_ajax_callback',
    ),
  );

  $form['views_pipe_settings']['message-wrapper'] = array(
    '#prefix' => '<div id="message-wrapper">',
    '#suffix' => '</div>',
  );

  // Set the message.
  $form['views_pipe_settings']['message-wrapper']['message'] = array(
    '#markup' => $message,
    '#prefix' => "<div class=\"messages $type\">",
    '#suffix' => '</div>',
  );

  $form['more'] = array(
    '#type' => 'fieldset',
    '#title' => t('More'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['more']['enabled'] = array(
    '#title' => t('Enabled'),
    '#type' => 'checkboxes',
    '#options' => $enabled_options,
    '#default_value' => $enabled_default,
  );

  $form['more']['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#rows' => 3,
    '#default_value' => $is_edit_or_clone_form ? $views_pipe['description'] : '',
  );

  $form['#submit'][] = 'views_pipes_views_pipe_form_submit';
  return system_settings_form($form);
}

/**
 * Ajax callback updating the select box section of the views pipe form.
 *
 * @see views_pipes_views_pipe_form()
 */
function views_pipes_settings_ajax_callback(&$form, &$form_state) {
  $commands = array();
  $commands[] = ajax_command_replace('#settings-wrapper', drupal_render($form['views_pipe_settings']['settings-wrapper']));
  $commands[] = ajax_command_replace('#message-wrapper', drupal_render($form['views_pipe_settings']['message-wrapper']));
  // Remove validation status message after ajax updates.
  $commands[] = ajax_command_remove('div#console');
  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Form validation handler for views_pipes_views_pipe_form().
 *
 * Perform the following checks:
 * a) Has an output field been selected.
 * b) Has an input argument been selected.
 * c) Does the output field type match the argument input type.
 *
 * @todo Are there any infinite feedback loops.
 *
 * @see views_pipes_views_pipe_form_submit()
 */
function views_pipes_views_pipe_form_validate(&$form, &$form_state) {
  if (isset($form_state['values']['no_results'])) {
    if (isset($form_state['values']['no_fields'])) {
      form_set_error('output_view', t('No output view results. There are currently no tables with fields. <em>You must change your views settings</em>.'));
      form_set_error('output_display');
      form_set_error('output_field');
    }
    if (isset($form_state['values']['no_arguments'])) {
      form_set_error('input_view',
        t('No input view results. There are no currently views with contextual filters set.') .
        '<em>' . t('You must change your views settings.') . '</em>');
      form_set_error('input_display');
      form_set_error('input_argument');
    }
  }
  else {
    $output_field_type = $form_state['values']['output_field_type'];
    $input_argument_type = $form_state['values']['input_argument_type'];
    if ($output_field_type != $input_argument_type) {
      $error_message = t("The output field <strong>@field_type</strong> and input argument <strong>@argument_type</strong> must match.<br /> If you feel this constraint is too restrictive you may consider using relationships.<br /> For example, if you are attempting to send output from content author <strong>node:uid</strong> to user id <strong>users:uid</strong> then add the <em>'Content: Author'</em> relationship to your output view and use it to add the <em>'User: Uid'</em> field (which can be hidden, if necessary).", array('@field_type' => $output_field_type, '@argument_type' => $input_argument_type));
      form_set_error('output_field', $error_message);
      form_set_error('input_argument');
    }
  }
  drupal_set_title($form_state['values']['page_title'], PASS_THROUGH);
}

/**
 * Form submission handler for views_pipes_views_pipe_form().
 *
 * @see views_pipes_views_pipe_form_validate()
 */
function views_pipes_views_pipe_form_submit($form, &$form_state) {
  module_load_include('inc', 'views_pipes', 'views_pipes');
  $v = $form_state['values'];
  // $v['enabled']['active'] is either 'active' or 0
  $active = strlen(sprintf("%s", $v['enabled']['active'])) == 1 ? 0 : 1;
  $data = array(
    'name' => $v['name'],
    'description' => $v['description'],
    'output_view' => $v['output_view'],
    'output_display' => $v['output_display'],
    'output_field' => $v['output_field'],
    'input_view' => $v['input_view'],
    'input_display' => $v['input_display'],
    'input_argument' => $v['input_argument'],
    'active' => $active,
  );
  if (isset($v['vpid'])) {
    views_pipes_db_update($v['vpid'], $data);
  }
  else {
    db_insert('views_pipes')->fields($data)->execute();
  }
  $form_state['redirect'] = 'admin/structure/views_pipes';
}

/**
 * Form builder to list and manage views pipes.
 *
 * @ingroup forms
 *
 * @see views_pipes_overview_submit()
 * @see theme_views_pipes_overview()
 */
function views_pipes_overview($form, $form_state) {
  drupal_set_title(t('Views Pipes Overview'));
  $views_pipes = db_select('views_pipes', 'v')->fields('v')->orderBy('weight')->execute()->fetchAll();
  $form['#tree'] = TRUE;
  foreach ($views_pipes as $views_pipe) {
    $output_settings = $views_pipe->output_view . ", " . $views_pipe->output_display . ", " . $views_pipe->output_field;
    $input_settings = $views_pipe->input_view . ", " . $views_pipe->input_display . ", " . $views_pipe->input_argument;

    $form[$views_pipe->vpid]['#views_pipe'] = $views_pipe;

    $form[$views_pipe->vpid]['name'] = array(
      '#markup' => check_plain($views_pipe->name),
    );

    $form[$views_pipe->vpid]['weight'] = array(
      '#type' => 'weight',
      '#title' => t('Weight for @title', array(
        '@title' => $views_pipe->name,
      )),
      '#title_display' => 'invisible',
      '#delta' => 10,
      '#default_value' => $views_pipe->weight,
    );

    $form[$views_pipe->vpid]['output_view'] = array(
      '#markup' => check_plain($output_settings),
    );

    $form[$views_pipe->vpid]['input_view'] = array(
      '#markup' => check_plain($input_settings),
    );

    $form[$views_pipe->vpid]['edit'] = array(
      '#type' => 'link',
      '#title' => t('edit'),
      '#href' => "admin/structure/views_pipes/$views_pipe->vpid/edit",
    );

    $enable_link = $views_pipe->active ? t('disable') : t('enable');
    $form[$views_pipe->vpid]['enable'] = array(
      '#type' => 'link',
      '#title' => $enable_link,
      '#href' => "admin/structure/views_pipes/$views_pipe->vpid/toggle-enabled",
    );

    $form[$views_pipe->vpid]['clone'] = array(
      '#type' => 'link',
      '#title' => t('clone'),
      '#href' => "admin/structure/views_pipes/$views_pipe->vpid/clone",
    );

    $form[$views_pipe->vpid]['delete'] = array(
      '#type' => 'link',
      '#title' => t('delete'),
      '#href' => "admin/structure/views_pipes/$views_pipe->vpid/delete",
    );
  }

  // Ensure weight is not displayed when there is only one item.
  if (count($views_pipes) > 1) {
    $form['actions'] = array(
      '#type' => 'actions',
    );

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
  }
  elseif (isset($views_pipe)) {
    unset($form[$views_pipe->vpid]['weight']);
  }
  return $form;
}

/**
 * Form submission handler for views_pipes_overview().
 */
function views_pipes_overview_submit($form, &$form_state) {
  module_load_include('inc', 'views_pipes', 'views_pipes');
  foreach ($form_state['values'] as $vpid => $views_pipe) {
    // If weight has changed.
    if (is_numeric($vpid) && $form[$vpid]['#views_pipe']->weight != $form_state['values'][$vpid]['weight']) {
      $form[$vpid]['#views_pipe']->weight = $form_state['values'][$vpid]['weight'];
      views_pipes_db_update($vpid, $form_state['values'][$vpid]);
    }
  }
  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Provides theming layer for the overview page.
 */
function theme_views_pipes_overview($variables) {
  $form = $variables['form'];
  $rows = array();

  foreach (element_children($form) as $key) {
    if (isset($form[$key]['name'])) {
      $views_pipe = &$form[$key];

      $row = array();
      $row[] = drupal_render($views_pipe['name']);
      if (isset($views_pipe['weight'])) {
        $views_pipe['weight']['#attributes']['class'] = array(
          'views-pipes-weight',
        );
        $row[] = drupal_render($views_pipe['weight']);
      }
      $row[] = drupal_render($views_pipe['output_view']);
      $row[] = drupal_render($views_pipe['input_view']);
      $row[] = drupal_render($views_pipe['edit']);
      $row[] = drupal_render($views_pipe['enable']);
      $row[] = drupal_render($views_pipe['clone']);
      $row[] = drupal_render($views_pipe['delete']);
      $rows[] = array('data' => $row, 'class' => array('draggable'));
    }
  }

  $header = array(t('Views Pipe name'));
  if (isset($form['actions'])) {
    $header[] = t('Weight');
    drupal_add_tabledrag('views-pipes', 'order', 'sibling', 'views-pipes-weight');
  }
  $header[] = array('data' => t('Output View'));
  $header[] = array('data' => t('Input View'));
  $header[] = array('data' => t('Operations'), 'colspan' => '4');
  return theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'empty' => t('None defined. <a href="@link">Add views pipe</a>.', array(
      '@link' => url('admin/structure/views_pipes/add'),
    )),
    'attributes' => array('id' => 'views-pipes'),
  )) . drupal_render_children($form);
}

/**
 * Act on user confirmation of delete views pipe operation.
 *
 * @param array $form
 *   The form array for confirming deletion of a views pipe.
 * @param array $form_state
 *   An array containing the current state of the form.
 * @param int $vpid
 *   Views Pipe DB id.
 */
function views_pipes_delete_confirm(array $form, array &$form_state, $vpid) {
  module_load_include('inc', 'views_pipes', 'views_pipes');
  $form['#vpid'] = $vpid;
  $form['vpid'] = array(
    '#type' => 'value',
    '#value' => $vpid,
  );

  $views_pipe = views_pipes_get_views_pipe($vpid);
  return confirm_form($form, t("Are you sure you want to delete '%name'?", array(
    '%name' => $views_pipe['name'],
  )), 'admin/structure/views_pipes', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Ask for user confirmation of delete views pipe operation.
 *
 * @param array $form
 *   The form array for confirming deletion of a views pipe.
 * @param array $form_state
 *   An array containing the current state of the form.
 */
function views_pipes_delete_confirm_submit(array $form, array &$form_state) {
  module_load_include('inc', 'views_pipes', 'views_pipes');
  if ($form_state['values']['confirm']) {
    $vpid = $form_state['values']['vpid'];
    $views_pipe = views_pipes_get_views_pipe($vpid);
    db_delete('views_pipes')->condition('vpid', $vpid)->execute();
    drupal_set_message(t('Views Pipe %name has been deleted.', array(
      '%name' => $views_pipe['name'],
    )));
  }
  $form_state['redirect'] = 'admin/structure/views_pipes';
}

/**
 * Ask for user confirmation of enable/disable views pipe operation.
 *
 * @param array $form
 *   The enable/disable form array.
 * @param array $form_state
 *   An array containing the current state of the form.
 * @param int $vpid
 *   Views Pipe DB id.
 */
function views_pipes_toggle_enabled_confirm(array $form, array &$form_state, $vpid) {
  module_load_include('inc', 'views_pipes', 'views_pipes');
  $form['#vpid'] = $vpid;
  $form['vpid'] = array(
    '#type' => 'value',
    '#value' => $vpid,
  );

  $views_pipe = views_pipes_get_views_pipe($vpid);
  $status = $views_pipe['active'] ? t('disable') : t('enable');

  return confirm_form($form, t("Are you sure you want to @status '%name'?", array(
    '@status' => $status,
    '%name' => $views_pipe['name'],
  )), 'admin/structure/views_pipes', '', ucfirst($status), t('Cancel'));
}

/**
 * Act on user confirmation of enable/disable views pipe operation.
 *
 * @param array $form
 *   The enable/disable form array.
 * @param array $form_state
 *   An array containing the current state of the form.
 */
function views_pipes_toggle_enabled_confirm_submit(array $form, array &$form_state) {
  module_load_include('inc', 'views_pipes', 'views_pipes');
  if ($form_state['values']['confirm']) {
    $vpid = $form_state['values']['vpid'];
    $views_pipe = views_pipes_get_views_pipe($vpid);
    $views_pipe['active'] = $views_pipe['active'] ? 0 : 1;
    $status = $views_pipe['active'] ? t('enabled') : t('disabled');
    views_pipes_db_update($vpid, $views_pipe);
    drupal_set_message(t('%name has been @status.',
    array(
      '%name' => $views_pipe['name'],
      '@status' => $status,
    )
    ));
  }
  $form_state['redirect'] = 'admin/structure/views_pipes';
}
